document.addEventListener('DOMContentLoaded', () => {
    var example = new Vue({
        el: '#example',
        data: {
            empname: '',
            empid: '',
            res: '',
            employees: []
        },
        methods: {
            submit: function () {
                console.log(this.empname, this.empid)
                let vm = this;
                axios.post('/user', {
                    empname: this.empname,
                    empid: this.empid
                })
                .then((response) => {
                    vm.res = response.data
                })
            },
            getAllEmployees: function() {
                let vm = this
                axios.get('/users')
                .then((response) => {
                    vm.employees = response.data
                })
            }
        }
    })
})