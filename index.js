const express = require('express')
const PORT = 3000

const app = express()


// GET
app.get('/', (req, res) => {
  res.send('Received a GET request on Path /')
})

// POST
app.post('/user', (req, res) => {
  res.send('Received a POST request on Path /user')
})

// PUT
app.put('/user', (req, res) => {
  res.send('Received a PUT request on Path /user')
})

// DELETE
app.delete('/user', (req, res) => {
  res.send('Received a DELETE request on Path /user')
})

// start server
app.listen(3000, () => {
  console.log(`App listening on port ${PORT}`);
})