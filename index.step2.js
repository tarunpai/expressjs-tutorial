const express = require('express')
const bodyParser = require('body-parser')
const mongoClient = require('mongodb').MongoClient

let db = {
  username: 'admin',
  password: 'admin123',
  instance: null
}
const connectionString = `mongodb://${db.username}:${db.password}@ds155278.mlab.com:55278/employees`
const PORT = 3000

const app = express()

app.use(express.static('public'))
app.use(bodyParser.json())


// GET
app.get('/users', (req, res) => {
  try {
    db.instance.collection('employee-list').find().toArray((err, response) => {
      if (err) res.send(err)

      res.send(response)
    })
  }
  catch (err) {
    console.log(err)
  }
})

// POST
app.post('/user', (req, res) => {
  db.instance.collection('employee-list').save(req.body, (err, response) => {
    if (err) res.send('error');

    res.send('Success! User has been saved to the DB')
  })
})

// PUT
app.put('/user', (req, res) => {
  res.send('Received a PUT request on Path /user')
})

// DELETE
app.delete('/user', (req, res) => {
  res.send('Received a DELETE request on Path /user')
})


// connect to mongodb
mongoClient.connect(connectionString, (err, client) => {
  if (err) return console.log(err);

  db.instance = client.db('employees')
  // start server
  app.listen(3000, () => {
    console.log(`App listening on port ${PORT}`);
  })
})